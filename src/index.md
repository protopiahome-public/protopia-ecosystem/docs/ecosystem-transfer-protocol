# Ecosystem Transfer Protocol

**Ecosystem Transfer Protocol** - протокол передачи данных, описывающий распределенную микросервисную реактивную архитектуру (экосистемную архитектуру).

## Авторы

_Моросеев Федор_

## Статус

Данный документ находиться в стадии черновика.

## Язык

Ключевые слова «ДОЛЖЕН»(MUST, SHALL, REQUIRED), «НЕ ДОЛЖЕН»(MUST NOT, SHALL NOT), «СЛЕДУЕТ»(SHOULD, RECOMMENDED), «НЕ СЛЕДУЕТ»(SHOULD NOT) и «ВОЗМОЖНО»(MAY, OPTIONAL) в этом документе следует интерпретировать так, как описано в [RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119) и его более новой версии [RFC 8174](https://datatracker.ietf.org/doc/html/rfc8174).

## Краткое описание

Ecosystem Transfer Protocol описывает распределенную микросервисную реактивную архитектуру, назначение узлов, протоколы передачи данных, протоколы авторизации, роль пользователя в этой архитектуре.


## Список спецификаций

Протокол состоит из:

1. [Ядро](draft-etp-core.md)
2. [Пользователь](draft-etp-user.md)
3. [Микросервисы](draft-etp-microservice-and-client.md)
4. [Graphql диалект](draft-etp-graphql-dialect.md)
5. [Токен](draft-etp-tokens.md)
6. [Авторизация](draft-etp-authorization.md)
